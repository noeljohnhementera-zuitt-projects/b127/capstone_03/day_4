import { Fragment, useState, useEffect, useContext } from 'react';

import { Form, Button, Row, Col } from 'react-bootstrap';

import { Redirect, useHistory } from 'react-router-dom';

import GlobalDataContext from '../GlobalDataContext';

// Import stylish alert
import Swal from 'sweetalert2';

export default function Register () {

	// create useHistory() variable
	const history = useHistory();

	// useContext to get user(getter) in UserContext.js
	const { user } = useContext(GlobalDataContext)

	// useState() for Button
	const [ isActive, setIsActive ] = useState(false);

	// useState() for inputs
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ address, setAddress ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ paymentOption, setPaymentOption ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	useEffect(() =>{
		if((firstName !== '' && lastName !== '' && address !== '' && email !== '' && paymentOption !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [firstName, lastName, address, email, paymentOption, password, verifyPassword])

	function registerUser(e){ // e or parameter e is required when making an event listener / virtual DOM
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					Swal.fire({
						title: 'Oops!',
						icon: 'error',
						text: 'Email already exists!'
					})
				}else{
					fetch('http://localhost:4000/users/register', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify ({
							firstName: firstName,
							lastName: lastName,
							address: address,
							email: email,
							paymentOption: paymentOption,
							password: password
						})
					})
						.then(res => res.json())
						.then(data => {
							console.log(data)
							if(data === false){

								Swal.fire({
									title: "Oops!",
									icon: "error",
									text: "Please check your inputs!"
								})
								
							}else{
								Swal.fire({
									title: "Success!",
									icon: "success",
									text: "You have successfully registered!"
								})
									history.push('/login')
							}
						})
					}
				})
			}

	return (
			(user.customerAccessToken !== null) ?
				<Redirect to = '/' />
			    :
		<Fragment>		
			<h1 className='text-center padding-top-5'>Register</h1>
			<Row className='justify-content-center'>
				<Col md={{ span: 6 }}>
					<Form onSubmit={(e) => registerUser(e)}>
						<Form.Group>
							<Form.Label>First Name:</Form.Label>
							<Form.Control 
								type='text'
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								placeholder='Enter Your First Name'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Last Name:</Form.Label>
							<Form.Control 
								type='text'
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								placeholder='Enter Your Last Name'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Address:</Form.Label>
							<Form.Control 
								type='text'
								value={address}
								onChange={e => setAddress(e.target.value)}
								placeholder='Enter Your Address'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Email:</Form.Label>
							<Form.Control 
								type='email'
								value={email}
								onChange={e =>setEmail(e.target.value)}
								placeholder='Enter Your Email'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Payment Option:</Form.Label>
							<Form.Control 
								type='text'
								value={paymentOption}
								onChange={e => setPaymentOption(e.target.value)}
								placeholder='Enter Your Payment Option'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control 
								type='password'
								value={password}
								onChange={e => setPassword(e.target.value)}
								placeholder='Enter Your Password'
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Verify Password:</Form.Label>
							<Form.Control 
								type='password'
								value={verifyPassword}
								onChange={e => setVerifyPassword(e.target.value)}
								placeholder='Verify Your Password'
								required
							/>
						</Form.Group>
						{isActive ?
						<Button variant='primary' type='submit'>Register</Button>
						:
						<Button variant='secondary' type='submit' disabled>Register</Button>
						}
					</Form>
				</Col>
			</Row>
		</Fragment>
	)
}