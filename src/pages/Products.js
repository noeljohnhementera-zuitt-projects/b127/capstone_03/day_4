import { useContext, useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import GlobalDataContext from '../GlobalDataContext';

import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Products () {
		// useContext() to determine if user is admin or not
		const { user } = useContext(GlobalDataContext);

		// fetched data will be stored in this useState()
		const [ allProducts, setAllProducts ] = useState([])

		const getAllProducts = () =>{
			fetch('http://localhost:4000/products/all')
			.then(res => res.json())
			.then(data => {
				setAllProducts(data)
				console.log(data)
				// to check if its working
			})
		}

		// to reuse the getAllProducts() function and avoid hard refresh
		useEffect(()=>{
			getAllProducts()
		}, [])

	return (
		<Container>
			{
				(user.isAdmin === true) ?
				<AdminView  productData={allProducts} getAllProducts={getAllProducts}/>
				:
				<UserView productData={allProducts}/>
			}	
		</Container>
	)
}
//productData is a paramameter to pass the new value of getter