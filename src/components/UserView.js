import { useState, useEffect, Fragment } from 'react';

import ProductCard from './ProductCard'

// Products = parent (passes data)
// UserView = child (uses the data)
export default function UserView ( { productData } ) {

	const [ products, setProducts ] = useState([])
	console.log(productData)

	useEffect(() =>{

		const getAllProducts = productData.map(eachProduct =>{
			if(eachProduct.isActive === true){
				return (
					<ProductCard productProp={eachProduct} key={eachProduct._id}/>
					// productProp will be used in child component, ProductCard
				)
			}else{
				return null
			}
		})

		setProducts(getAllProducts)

	}, [productData])

	return (
		<Fragment>
			{products}
		</Fragment>
	)
}
// setter setProducts(getAllProducts) wc was set in useEffect() will be the new value of the getter, products