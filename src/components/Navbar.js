// First Step Imports
import { Navbar, Nav } from 'react-bootstrap';

// Import routers
import { Link, NavLink } from 'react-router-dom';

// import react
import { Fragment, useContext } from 'react';

// import react Context
import GlobalDataContext from '../GlobalDataContext';


export default function AppNavbar () {

	const { user } = useContext(GlobalDataContext);

	const changeNav = (user.customerAccessToken !== null) ?
		<Fragment>
			<Nav.Link as={NavLink} to='/logout'> Logout </Nav.Link>
		</Fragment>
		:
		<Fragment>
			<Nav.Link as={NavLink} to='/login'> Login </Nav.Link>
			<Nav.Link as={NavLink} to='/register'> Register </Nav.Link>
		</Fragment>

	return (
		<Navbar bg='primary' variant='dark' expand='lg'>
			<Navbar.Brand as={Link} to='/'>All-Resto-E-Commerce</Navbar.Brand>
			<Navbar.Toggle aria-controls='basic-navbar-nav'/>
			<Navbar.Collapse id='basic-navbar-nav'>
				<Nav className='ml-auto'>
					<Nav.Link as={NavLink} to='/'>Home</Nav.Link>
					<Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
					<Nav.Link as={NavLink} to='/reviews'>Reviews</Nav.Link>
					{changeNav}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}