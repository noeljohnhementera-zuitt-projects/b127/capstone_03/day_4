import { useState, useEffect, Fragment } from 'react';

import { Table, Button, Modal, Form, Container } from 'react-bootstrap';

import Swal from 'sweetalert2';
// Products = parent (passes data)
// AdminView = child (uses the data)
export default function AdminView (props) {

	// use the data from parent component, Products
	const { productData, getAllProducts } = props
	console.log(props)

	const [ products, setProducts ] = useState([])

	// addProduct -> create useState() to store data when adding a product
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	// addProduct -> useState() for addProduct modal button
	const [ showModal, setShowModal ] = useState(false);
	// addProduct -> functions handling opening and closing of addProduct modal
	const openModal = () => setShowModal(true);
	const closeModal = () => setShowModal(false);

	// updateProductButton -> useState() for updateProductButton modal button
	const [ showEditProductModal, setShowEditProductModal ] = useState(false);
	// openEditModal -> useState() to get the fetched data and become the setter or new value of the _id
	const [ getProductId, setGetProductId ] = useState('');
	
	// updateProductButton -> functions handling opening and closing of updateProductButton modal
	const openEditModal = (productId) =>{
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			// use setter in useState to change the update the properties of an object
			setGetProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		// open the modal
		setShowEditProductModal(true)
	}

	const closeEditModal = () =>{
		// close the modal
		setShowEditProductModal(false)
		// reset the values
		setName('')
		setDescription('')
		setPrice(0)
	}

	useEffect(()=>{
		const getAllProducts = productData.map(eachProduct =>{
			return (
			<tr	key={eachProduct._id}>
				<td>{eachProduct._id}</td>
				<td>{eachProduct.name}</td>
				<td>{eachProduct.description}</td>
				<td>{eachProduct.price}</td>
				<td className={eachProduct.isActive ? 'text-success' : 'text-danger'}> {eachProduct.isActive ? 'Available' : "Unavailable"}</td>
				<td>
					<Container className='d-flex justify-content-center'>
						<Button variant='primary' size='sm' onClick={() => openEditModal(eachProduct._id)}>Update</Button>
						{eachProduct.isActive ?
						<Button variant='danger' size='sm' onClick={() => archiveProductButton(eachProduct._id, eachProduct.isActive)}>Archive</Button>
						:
						<Button variant='success' size='sm' onClick={() => enableProductButton(eachProduct._id, eachProduct.isActive)}>Enable</Button>
						}
					</Container>
				</td>
			</tr>
			)
		})

		setProducts(getAllProducts)
	}, [productData])

	// Add a product
	const addProduct = (e) =>{
		e.preventDefault();
		fetch('http://localhost:4000/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				// need token bec only an admin user is allowed to add course
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
			.then(res => res.json())
			.then(data =>{
				console.log(data)
				if(data === false){
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: 'Please try again!'
					})
					
				}else{
					getAllProducts()
					// props that we collected sa parent component (Products.js)
					Swal.fire({
						title: "Success!",
						icon: 'success',
						text: 'Course successfully added!'
					})

					// reset inputs after successful prompt
					setName('')
					setDescription('')
					setPrice(0)

					// to automatically close the modal
					closeModal()
				}
			})
	}

	// Edit a Product
	const updateProductButton = (e, productId) =>{
		e.preventDefault();
		fetch(`http://localhost:4000/products/${productId}/updated-successfully`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					getAllProducts()
					Swal.fire({
						title: "Updated Successfully!",
						icon: 'success',
						text: 'Product successfully updated!'
					})
					closeEditModal()
				}else{
					getAllProducts()
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: 'Please try again!'
					})
				}
			})

	}

	// Enable a Product
	const enableProductButton = (productId, isActive) =>{
		fetch(`http://localhost:4000/products/${ productId }/enabled-successfully`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('customerAccessToken')}`
				// this is the token from Inspect > Application (localStorage.setItem sa Login.js)
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
			.then(res => res.json())
			.then(data =>{
				if(data === true){
					getAllProducts()
					Swal.fire({
						title: "Enabled Successfully!",
						icon: 'success',
						text: 'Product successfully enabled!'
					})
				}else{
					getAllProducts()
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: 'Please try again!'
					})
				}
			})
	}

	// Archive a Product
	const archiveProductButton = (productId, isActive) =>{
		fetch(`http://localhost:4000/products/${ productId }/archived-successfully`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('customerAccessToken')}`
				// this is the token from Inspect > Application (localStorage.setItem sa Login.js)
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
			.then(res => res.json())
			.then(data =>{
				if(data === false){
					
					getAllProducts()
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: 'Please try again!'
					})
				}else{
					getAllProducts()
					Swal.fire({
						title: "Archived Successfully!",
						icon: 'success',
						text: 'Product successfully archived!'
					})
				}
			})
	}

	return (
		<Fragment>
			<div className='my-4 text-center'>
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant='primary' onClick={openModal}> Add A Product</Button>
				</div>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr className='text-center'>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

		{/*addProduct Modal*/}
			<Modal show={showModal} onHide={closeModal}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control 
								type='text'
								value={name}
								onChange={e => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control 
								type='text'
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control 
								type='number'
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeModal}>Close</Button>
						<Button variant='success' type='submit'>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*Edit a Product Modal*/}
			<Modal show={showEditProductModal} onHide={closeEditModal}>
				<Form onSubmit={e => updateProductButton(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control 
								type='text'
								value={name}
								onChange={e => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control 
								type='text'
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control 
								type='number'
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeEditModal}>Close</Button>
						<Button variant='success' type='submit'>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</Fragment>
	)
}