import { Row, Col, Card, Button } from 'react-bootstrap';

import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

export default function ProductCard ( { productProp } ) {

	const { _id, name, description, price } = productProp;

	return (
		<Row>
			<Col>
				<Card>
					<Card.Body>
						<Card.Title><h2>{name}</h2></Card.Title>
						<Link className='btn btn-primary' to={`/products/${_id}`}>See Details</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}