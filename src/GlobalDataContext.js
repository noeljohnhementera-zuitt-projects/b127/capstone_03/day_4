import React from 'react';

export default React.createContext();
// I'll use this to pass any data globally that uses the useContext() hook
